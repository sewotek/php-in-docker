# PHP simple web app

PHP is quite popular technology.

We are using PHP in our main product - you should know how to make images for PHP applications.

## Your task

Build image serving simple PHP application.

After executing

    docker run --rm -ti -p 80:80 -e VERSION=0.1 yourdockerid/image-name:tag

target web page should display something like this:

---

## Hello from Docker!

**DevOps:** your@email.address

Container host: 89a755dfa10b

App version: 0.1

---

## Requirements

- you can't modify PHP code,
- at /devops web path container should show your email,
- at /version web path container should show provided version,

## Remarks
- want impress us more? let container log speak in your name - what is going on, errors, requests, etc.

